<?xml version="1.0" encoding="UTF-8"?>
<tileset name="castlewalls" tilewidth="32" tileheight="32" tilecount="180" columns="12">
 <image source="castlewalls.png" width="384" height="480"/>
 <terraintypes>
  <terrain name="New Terrain" tile="14"/>
 </terraintypes>
 <tile id="1" terrain="0,0,,0"/>
 <tile id="2" terrain=",,0,0"/>
 <tile id="3" terrain="0,0,0,"/>
 <tile id="13" terrain=",0,,0"/>
 <tile id="14" terrain="0,0,0,0"/>
 <tile id="15" terrain="0,,0,"/>
 <tile id="25" terrain=",0,0,0"/>
 <tile id="26" terrain="0,0,,"/>
 <tile id="27" terrain="0,,0,0"/>
</tileset>
