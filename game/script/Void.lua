local levity = require "levity"
local love_graphics_rectangle = love.graphics.rectangle
local love_graphics_stencil = love.graphics.stencil
local love_graphics_setStencilTest = love.graphics.setStencilTest
local love_math_random = love.math.random

local Void = class()
function Void:_init(layer)
	layer.visible = true
	self.safeareas = levity.map.layers["safeareas"].objects
	local safelimitlayer = levity.map.layers["safelimit"]
	local safelimits = safelimitlayer and safelimitlayer.objects
	local safelimit = safelimits and safelimits[1]
	if safelimit then
		self.safelimitx1 = safelimit.x
		self.safelimity1 = safelimit.y
		self.safelimitx2 = safelimit.x + safelimit.width
		self.safelimity2 = safelimit.y + safelimit.height
	else
		self.safelimitx1 = 0
		self.safelimity1 = 0
		self.safelimitx2 = levity.map.width*levity.map.tilewidth
		self.safelimity2 = levity.map.height*levity.map.tileheight
	end
end

function Void:randomSafeAreaPoint()
	local safeareas = self.safeareas
	local safearea = safeareas[love_math_random(#safeareas)]
	return love_math_random(safearea.x, safearea.x + safearea.width),
		love_math_random(safearea.y, safearea.y + safearea.height)
end

function Void:getSafeArea(x, y)
	for _, safearea in pairs(self.safeareas) do
		local id = safearea.id
		if levity.scripts:call(id, "testPoint", x, y) then
			return id
		end
	end
end

function Void:isFullyInSafeArea(x1, y1, x2, y2)
	for _, safearea in pairs(self.safeareas) do
		local id = safearea.id
		local co = levity.scripts:call(id, "testBounds", x1, y1, x2, y2)
		if co ~= "inside" then
			return false
		end
	end
	return true
end
local isFullyInSafeArea = Void.isFullyInSafeArea

function Void:isBodyFullyInSafeArea(body)
	for _, fixture in pairs(body:getFixtureList()) do
		if not isFullyInSafeArea(self, fixture:getBoundingBox()) then
			return false
		end
	end
	return true
end

function Void:findSafeSpawn(minw, minh)
	for _, safearea in pairs(self.safeareas) do
		local id = safearea.id
		local x, y = levity.scripts:call(id, "getSafeSpawn", minw, minh)
		if x and y then
			return x, y
		end
	end
end

function Void:growSafeAreas(dt)
	local safelimitx1 = self.safelimitx1
	local safelimity1 = self.safelimity1
	local safelimitx2 = self.safelimitx2
	local safelimity2 = self.safelimity2
	for _, safearea in pairs(self.safeareas) do
		local id = safearea.id
		levity.scripts:send(id, "grow", dt, safelimitx1, safelimity1,
					safelimitx2, safelimity2)
	end
end

local function drawStencil()
	local safeareas = levity.map.layers["safeareas"].objects
	for _, object in pairs(safeareas) do
		love_graphics_rectangle("fill", object.x, object.y,
					object.width, object.height)
	end
end

function Void:drawUnder()
	love_graphics_stencil(drawStencil, "replace", 1, false)
	love_graphics_setStencilTest("lequal", 0)
end

function Void:drawOver()
	love_graphics_setStencilTest()
	--[[
	local safeareas = levity.map.layers["safeareas"].objects
	for _, object in pairs(safeareas) do
		love_graphics_rectangle("line", object.x, object.y,
					object.width, object.height)
	end
	]]
end

return Void
