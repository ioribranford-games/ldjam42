local levity = require "levity"

local Bullet = class()
function Bullet:_init(bullet)
	self.bullet = bullet
end

function Bullet:hitSomething(damage)
	local bullet = self.bullet

	local body = bullet.body
	body:setLinearVelocity(0, 0)

	local properties = bullet.properties
	local tileset = bullet.tile.tileset
	local hittileid = properties.hittileid
	local gid = levity.map:getTileGid(tileset, hittileid)

	if hittileid then
		if gid ~= bullet.gid then
			bullet:setTileId(hittileid)
			levity.bank:play(damage and properties.hitdamagesound
					or properties.hitnodamagesound)
		end
	else
		levity:discardObject(bullet.id)
	end
end

function Bullet:endMove(dt)
	local bullet = self.bullet
	local body = bullet.body
	if not levity.scripts:call("void", "isBodyFullyInSafeArea", body) then
		self:hitSomething(false)
	end
end

return Bullet
