return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "1.1.6",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 15,
  height = 20,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 17,
  properties = {
    ["enemykillsfornextwave"] = 10,
    ["enemykillsfornextwaveinc"] = 2,
    ["enemyspawndist"] = 480,
    ["enemyspawndistinc"] = -20,
    ["enemyspawntime"] = 1.5,
    ["enemyspawntimeinc"] = -0.1,
    ["enemyspeedinc"] = 7.5,
    ["maxwave"] = 11,
    ["music"] = "03 - Main Table.vgz",
    ["nextwavegrowsafeareatime"] = 1,
    ["nextwavesound"] = "nextwave.ogg",
    ["nextwavewaittime"] = 1.5,
    ["playerid"] = 1,
    ["script"] = "Game"
  },
  tilesets = {
    {
      name = "castlefloors",
      firstgid = 1,
      filename = "castle/castlefloors.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "castle/castlefloors.png",
      imagewidth = 320,
      imageheight = 320,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {
        {
          name = "New Terrain",
          tile = 22,
          properties = {}
        },
        {
          name = "New Terrain",
          tile = 27,
          properties = {}
        },
        {
          name = "New Terrain",
          tile = 72,
          properties = {}
        },
        {
          name = "New Terrain",
          tile = 66,
          properties = {}
        },
        {
          name = "New Terrain",
          tile = 75,
          properties = {}
        }
      },
      tilecount = 100,
      tiles = {
        {
          id = 0,
          terrain = { 0, 0, 0, -1 }
        },
        {
          id = 4,
          terrain = { 0, 0, -1, 0 }
        },
        {
          id = 5,
          terrain = { -1, 1, 1, 1 }
        },
        {
          id = 9,
          terrain = { 1, -1, 1, 1 }
        },
        {
          id = 11,
          terrain = { -1, -1, -1, 0 }
        },
        {
          id = 12,
          terrain = { -1, -1, 0, 0 }
        },
        {
          id = 13,
          terrain = { -1, -1, 0, -1 }
        },
        {
          id = 16,
          terrain = { -1, -1, -1, 1 }
        },
        {
          id = 17,
          terrain = { -1, -1, 1, 1 }
        },
        {
          id = 18,
          terrain = { -1, -1, 1, -1 }
        },
        {
          id = 21,
          terrain = { -1, 0, -1, 0 }
        },
        {
          id = 22,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 23,
          terrain = { 0, -1, 0, -1 }
        },
        {
          id = 26,
          terrain = { -1, 1, -1, 1 }
        },
        {
          id = 27,
          terrain = { 1, 1, 1, 1 }
        },
        {
          id = 28,
          terrain = { 1, -1, 1, -1 }
        },
        {
          id = 31,
          terrain = { -1, 0, -1, -1 }
        },
        {
          id = 32,
          terrain = { 0, 0, -1, -1 }
        },
        {
          id = 33,
          terrain = { 0, -1, -1, -1 }
        },
        {
          id = 36,
          terrain = { -1, 1, -1, -1 }
        },
        {
          id = 37,
          terrain = { 1, 1, -1, -1 }
        },
        {
          id = 38,
          terrain = { 1, -1, -1, -1 }
        },
        {
          id = 40,
          terrain = { 0, -1, 0, 0 }
        },
        {
          id = 44,
          terrain = { -1, 0, 0, 0 }
        },
        {
          id = 45,
          terrain = { 1, 1, -1, 1 }
        },
        {
          id = 49,
          terrain = { 1, 1, 1, -1 }
        },
        {
          id = 50,
          terrain = { 2, 2, 2, -1 }
        },
        {
          id = 54,
          terrain = { 2, 2, -1, 2 }
        },
        {
          id = 55,
          terrain = { 4, 3, 3, 3 }
        },
        {
          id = 56,
          terrain = { 4, 4, 3, 3 }
        },
        {
          id = 57,
          terrain = { 3, 4, 3, 3 }
        },
        {
          id = 58,
          terrain = { 3, 4, 3, 4 }
        },
        {
          id = 61,
          terrain = { -1, -1, -1, 2 }
        },
        {
          id = 62,
          terrain = { -1, -1, 2, 2 }
        },
        {
          id = 63,
          terrain = { -1, -1, 2, -1 }
        },
        {
          id = 65,
          terrain = { 3, 3, 4, 3 }
        },
        {
          id = 66,
          terrain = { 3, 3, 3, 3 }
        },
        {
          id = 67,
          terrain = { 3, 3, 3, 4 }
        },
        {
          id = 68,
          terrain = { 4, 3, 4, 3 }
        },
        {
          id = 71,
          terrain = { -1, 2, -1, 2 }
        },
        {
          id = 72,
          terrain = { 2, 2, 2, 2 }
        },
        {
          id = 73,
          terrain = { 2, -1, 2, -1 }
        },
        {
          id = 75,
          terrain = { 4, 4, 4, 4 }
        },
        {
          id = 76,
          terrain = { 3, 3, 4, 4 }
        },
        {
          id = 81,
          terrain = { -1, 2, -1, -1 }
        },
        {
          id = 82,
          terrain = { 2, 2, -1, -1 }
        },
        {
          id = 83,
          terrain = { 2, -1, -1, -1 }
        },
        {
          id = 90,
          terrain = { 2, -1, 2, 2 }
        },
        {
          id = 94,
          terrain = { -1, 2, 2, 2 }
        }
      }
    },
    {
      name = "castlewalls",
      firstgid = 101,
      filename = "castle/castlewalls.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "castle/castlewalls.png",
      imagewidth = 384,
      imageheight = 480,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {
        {
          name = "New Terrain",
          tile = 14,
          properties = {}
        }
      },
      tilecount = 180,
      tiles = {
        {
          id = 1,
          terrain = { 0, 0, -1, 0 }
        },
        {
          id = 2,
          terrain = { -1, -1, 0, 0 }
        },
        {
          id = 3,
          terrain = { 0, 0, 0, -1 }
        },
        {
          id = 13,
          terrain = { -1, 0, -1, 0 }
        },
        {
          id = 14,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 15,
          terrain = { 0, -1, 0, -1 }
        },
        {
          id = 25,
          terrain = { -1, 0, 0, 0 }
        },
        {
          id = 26,
          terrain = { 0, 0, -1, -1 }
        },
        {
          id = 27,
          terrain = { 0, -1, 0, 0 }
        }
      }
    },
    {
      name = "candle",
      firstgid = 281,
      filename = "castle/candle.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "castle/candle.png",
      imagewidth = 128,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 8,
      tiles = {
        {
          id = 0,
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            }
          }
        },
        {
          id = 4,
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            }
          }
        }
      }
    },
    {
      name = "cabinets",
      firstgid = 289,
      filename = "castle/cabinets.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "castle/cabinets.png",
      imagewidth = 192,
      imageheight = 416,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 78,
      tiles = {}
    },
    {
      name = "dungeonex",
      firstgid = 367,
      filename = "castle/dungeonex.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "castle/dungeonex.png",
      imagewidth = 320,
      imageheight = 320,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 100,
      tiles = {}
    },
    {
      name = "cast",
      firstgid = 467,
      filename = "sorceress/cast.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "sorceress/cast.png",
      imagewidth = 448,
      imageheight = 256,
      tileoffset = {
        x = -32,
        y = 24
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {
        ["commonanimation"] = "south"
      },
      terrains = {},
      tilecount = 28,
      tiles = {
        {
          id = 0,
          type = "Player",
          properties = {
            ["name"] = "north"
          }
        },
        {
          id = 1,
          type = "Player"
        },
        {
          id = 2,
          type = "Player"
        },
        {
          id = 3,
          type = "Player"
        },
        {
          id = 4,
          type = "Player"
        },
        {
          id = 5,
          type = "Player"
        },
        {
          id = 6,
          type = "Player"
        },
        {
          id = 7,
          type = "Player",
          properties = {
            ["name"] = "west"
          }
        },
        {
          id = 8,
          type = "Player"
        },
        {
          id = 9,
          type = "Player"
        },
        {
          id = 10,
          type = "Player"
        },
        {
          id = 11,
          type = "Player"
        },
        {
          id = 12,
          type = "Player"
        },
        {
          id = 13,
          type = "Player"
        },
        {
          id = 14,
          type = "Player",
          properties = {
            ["name"] = "south"
          },
          animation = {
            {
              tileid = 14,
              duration = 66
            },
            {
              tileid = 15,
              duration = 66
            },
            {
              tileid = 16,
              duration = 66
            },
            {
              tileid = 17,
              duration = 66
            },
            {
              tileid = 18,
              duration = 66
            },
            {
              tileid = 19,
              duration = 604
            },
            {
              tileid = 20,
              duration = 66
            }
          }
        },
        {
          id = 15,
          type = "Player"
        },
        {
          id = 16,
          type = "Player"
        },
        {
          id = 17,
          type = "Player"
        },
        {
          id = 18,
          type = "Player"
        },
        {
          id = 19,
          type = "Player"
        },
        {
          id = 20,
          type = "Player"
        },
        {
          id = 21,
          type = "Player",
          properties = {
            ["name"] = "east"
          }
        },
        {
          id = 22,
          type = "Player"
        },
        {
          id = 23,
          type = "Player"
        },
        {
          id = 24,
          type = "Player"
        },
        {
          id = 25,
          type = "Player"
        },
        {
          id = 26,
          type = "Player"
        },
        {
          id = 27,
          type = "Player"
        }
      }
    },
    {
      name = "die",
      firstgid = 495,
      filename = "sorceress/die.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "sorceress/die.png",
      imagewidth = 192,
      imageheight = 128,
      tileoffset = {
        x = -32,
        y = 24
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 6,
      tiles = {
        {
          id = 0,
          type = "Player",
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 200
            },
            {
              tileid = 3,
              duration = 200
            },
            {
              tileid = 2,
              duration = 200
            },
            {
              tileid = 3,
              duration = 200
            },
            {
              tileid = 2,
              duration = 200
            },
            {
              tileid = 3,
              duration = 200
            },
            {
              tileid = 2,
              duration = 200
            },
            {
              tileid = 3,
              duration = 200
            },
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 1100
            }
          }
        },
        {
          id = 1,
          type = "Player"
        },
        {
          id = 2,
          type = "Player"
        },
        {
          id = 3,
          type = "Player"
        },
        {
          id = 4,
          type = "Player"
        },
        {
          id = 5,
          type = "Player"
        }
      }
    },
    {
      name = "swing",
      firstgid = 501,
      filename = "sorceress/swing.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "sorceress/swing.png",
      imagewidth = 384,
      imageheight = 256,
      tileoffset = {
        x = -32,
        y = 24
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {
        ["commonanimation"] = "south",
        ["commoncollision"] = "south"
      },
      terrains = {},
      tilecount = 24,
      tiles = {
        {
          id = 0,
          type = "Player",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "north"
          }
        },
        {
          id = 1,
          type = "Player",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 2,
          type = "Player",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Player",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Player",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 5,
          type = "Player",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Player",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "west"
          }
        },
        {
          id = 7,
          type = "Player",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 8,
          type = "Player",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 9,
          type = "Player",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 10,
          type = "Player",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 11,
          type = "Player",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Player",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "south"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "",
                shape = "rectangle",
                x = 24,
                y = 32,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 12,
              duration = 33
            },
            {
              tileid = 13,
              duration = 33
            },
            {
              tileid = 14,
              duration = 33
            },
            {
              tileid = 15,
              duration = 33
            },
            {
              tileid = 16,
              duration = 33
            },
            {
              tileid = 17,
              duration = 33
            }
          }
        },
        {
          id = 13,
          type = "Player",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 14,
          type = "Player",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 15,
          type = "Player",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 16,
          type = "Player",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 17,
          type = "Player",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 18,
          type = "Player",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "east"
          }
        },
        {
          id = 19,
          type = "Player",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 20,
          type = "Player",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 21,
          type = "Player",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 22,
          type = "Player",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 23,
          type = "Player",
          properties = {
            ["faceangle"] = 0
          }
        }
      }
    },
    {
      name = "walk",
      firstgid = 525,
      filename = "sorceress/walk.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "sorceress/walk.png",
      imagewidth = 576,
      imageheight = 256,
      tileoffset = {
        x = -32,
        y = 24
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {
        ["commonanimation"] = "walksouth",
        ["commoncollision"] = "standsouth"
      },
      terrains = {},
      tilecount = 36,
      tiles = {
        {
          id = 0,
          type = "Player",
          properties = {
            ["animated"] = false,
            ["faceangle"] = 270,
            ["name"] = "standnorth"
          }
        },
        {
          id = 1,
          type = "Player",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "walknorth"
          }
        },
        {
          id = 2,
          type = "Player",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Player",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Player",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 5,
          type = "Player",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Player",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 7,
          type = "Player",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 8,
          type = "Player",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 9,
          type = "Player",
          properties = {
            ["animated"] = false,
            ["faceangle"] = 180,
            ["name"] = "standwest"
          }
        },
        {
          id = 10,
          type = "Player",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "walkwest"
          }
        },
        {
          id = 11,
          type = "Player",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Player",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 13,
          type = "Player",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 14,
          type = "Player",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 15,
          type = "Player",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 16,
          type = "Player",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 17,
          type = "Player",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 18,
          type = "Player",
          properties = {
            ["animated"] = false,
            ["faceangle"] = 90,
            ["name"] = "standsouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 24,
                y = 32,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true
                }
              }
            }
          }
        },
        {
          id = 19,
          type = "Player",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "walksouth"
          },
          animation = {
            {
              tileid = 19,
              duration = 66
            },
            {
              tileid = 20,
              duration = 66
            },
            {
              tileid = 21,
              duration = 66
            },
            {
              tileid = 22,
              duration = 66
            },
            {
              tileid = 23,
              duration = 66
            },
            {
              tileid = 24,
              duration = 66
            },
            {
              tileid = 25,
              duration = 66
            },
            {
              tileid = 26,
              duration = 66
            }
          }
        },
        {
          id = 20,
          type = "Player",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 21,
          type = "Player",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 22,
          type = "Player",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 23,
          type = "Player",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 24,
          type = "Player",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 25,
          type = "Player",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 26,
          type = "Player",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 27,
          type = "Player",
          properties = {
            ["animated"] = false,
            ["faceangle"] = 0,
            ["name"] = "standeast"
          }
        },
        {
          id = 28,
          type = "Player",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "walkeast"
          }
        },
        {
          id = 29,
          type = "Player",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 30,
          type = "Player",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 31,
          type = "Player",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 32,
          type = "Player",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 33,
          type = "Player",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 34,
          type = "Player",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 35,
          type = "Player",
          properties = {
            ["faceangle"] = 0
          }
        }
      }
    },
    {
      name = "playershot",
      firstgid = 561,
      filename = "projectile/playershot.tsx",
      tilewidth = 32,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "projectile/playershot.png",
      imagewidth = 32,
      imageheight = 112,
      tileoffset = {
        x = -16,
        y = 8
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 16
      },
      properties = {
        ["commoncollision"] = "shot"
      },
      terrains = {},
      tilecount = 7,
      tiles = {
        {
          id = 0,
          type = "PlayerShot",
          properties = {
            ["name"] = "flash",
            ["nextanimtileid"] = "shot"
          },
          animation = {
            {
              tileid = 0,
              duration = 33
            },
            {
              tileid = 1,
              duration = 33
            }
          }
        },
        {
          id = 1,
          type = "PlayerShot",
          properties = {
            ["nextanimtileid"] = "shot"
          }
        },
        {
          id = 2,
          type = "PlayerShot",
          properties = {
            ["name"] = "shot"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 32,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 2,
              duration = 16
            },
            {
              tileid = 3,
              duration = 16
            },
            {
              tileid = 4,
              duration = 16
            },
            {
              tileid = 3,
              duration = 16
            }
          }
        },
        {
          id = 3,
          type = "PlayerShot"
        },
        {
          id = 4,
          type = "PlayerShot"
        },
        {
          id = 5,
          type = "PlayerShot",
          properties = {
            ["animenddiscard"] = true,
            ["name"] = "hit"
          },
          animation = {
            {
              tileid = 5,
              duration = 33
            },
            {
              tileid = 6,
              duration = 33
            }
          }
        },
        {
          id = 6,
          type = "PlayerShot",
          properties = {
            ["animenddiscard"] = true
          }
        }
      }
    },
    {
      name = "spider01",
      firstgid = 568,
      filename = "spider/spider01.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider01.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "attacknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 1,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 2,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "walknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 5,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 7,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 8,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 9,
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 10,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "attackwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 11,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 13,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 14,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "walkwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 15,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 16,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 17,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 18,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 19,
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 20,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "attacksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 21,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 22,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 23,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 24,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "walksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 25,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 26,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 27,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 28,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 29,
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 30,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "attackeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 31,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 32,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 33,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 34,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "walkeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 35,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 36,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 37,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 38,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 39,
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 40,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true,
            ["collidable"] = false,
            ["name"] = "die"
          },
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            }
          }
        },
        {
          id = 41,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 42,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 43,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 44,
          type = "Enemy"
        },
        {
          id = 45,
          type = "Enemy"
        },
        {
          id = 46,
          type = "Enemy"
        },
        {
          id = 47,
          type = "Enemy"
        },
        {
          id = 48,
          type = "Enemy"
        }
      }
    },
    {
      name = "spider02",
      firstgid = 618,
      filename = "spider/spider02.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider02.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "attacknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 1,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 2,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "walknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 5,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 7,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 8,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 9,
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 10,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "attackwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 11,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 13,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 14,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "walkwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 15,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 16,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 17,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 18,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 19,
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 20,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "attacksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 21,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 22,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 23,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 24,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "walksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 25,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 26,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 27,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 28,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 29,
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 30,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "attackeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 31,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 32,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 33,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 34,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "walkeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 35,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 36,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 37,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 38,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 39,
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 40,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true,
            ["collidable"] = false,
            ["name"] = "die"
          },
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            }
          }
        },
        {
          id = 41,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 42,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 43,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 44,
          type = "Enemy"
        },
        {
          id = 45,
          type = "Enemy"
        },
        {
          id = 46,
          type = "Enemy"
        },
        {
          id = 47,
          type = "Enemy"
        },
        {
          id = 48,
          type = "Enemy"
        }
      }
    },
    {
      name = "spider03",
      firstgid = 668,
      filename = "spider/spider03.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider03.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "attacknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 1,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 2,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "walknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 5,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 7,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 8,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 9,
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 10,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "attackwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 11,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 13,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 14,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "walkwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 15,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 16,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 17,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 18,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 19,
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 20,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "attacksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 21,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 22,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 23,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 24,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "walksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 25,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 26,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 27,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 28,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 29,
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 30,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "attackeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 31,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 32,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 33,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 34,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "walkeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 35,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 36,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 37,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 38,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 39,
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 40,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true,
            ["collidable"] = false,
            ["name"] = "die"
          },
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            }
          }
        },
        {
          id = 41,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 42,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 43,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 44,
          type = "Enemy"
        },
        {
          id = 45,
          type = "Enemy"
        },
        {
          id = 46,
          type = "Enemy"
        },
        {
          id = 47,
          type = "Enemy"
        },
        {
          id = 48,
          type = "Enemy"
        }
      }
    },
    {
      name = "spider04",
      firstgid = 718,
      filename = "spider/spider04.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider04.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "attacknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 1,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 2,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "walknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 5,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 7,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 8,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 9,
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 10,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "attackwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 11,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 13,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 14,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "walkwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 15,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 16,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 17,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 18,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 19,
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 20,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "attacksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 21,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 22,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 23,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 24,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "walksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 25,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 26,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 27,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 28,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 29,
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 30,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "attackeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 31,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 32,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 33,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 34,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "walkeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 35,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 36,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 37,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 38,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 39,
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 40,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true,
            ["collidable"] = false,
            ["name"] = "die"
          },
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            }
          }
        },
        {
          id = 41,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 42,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 43,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 44,
          type = "Enemy"
        },
        {
          id = 45,
          type = "Enemy"
        },
        {
          id = 46,
          type = "Enemy"
        },
        {
          id = 47,
          type = "Enemy"
        },
        {
          id = 48,
          type = "Enemy"
        }
      }
    },
    {
      name = "spider05",
      firstgid = 768,
      filename = "spider/spider05.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider05.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "attacknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 1,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 2,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "walknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 5,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 7,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 8,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 9,
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 10,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "attackwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 11,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 13,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 14,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "walkwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 15,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 16,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 17,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 18,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 19,
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 20,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "attacksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 21,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 22,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 23,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 24,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "walksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 25,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 26,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 27,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 28,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 29,
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 30,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "attackeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 31,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 32,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 33,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 34,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "walkeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 35,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 36,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 37,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 38,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 39,
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 40,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true,
            ["collidable"] = false,
            ["name"] = "die"
          },
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            }
          }
        },
        {
          id = 41,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 42,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 43,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 44,
          type = "Enemy"
        },
        {
          id = 45,
          type = "Enemy"
        },
        {
          id = 46,
          type = "Enemy"
        },
        {
          id = 47,
          type = "Enemy"
        },
        {
          id = 48,
          type = "Enemy"
        }
      }
    },
    {
      name = "spider06",
      firstgid = 818,
      filename = "spider/spider06.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider06.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "attacknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 1,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 2,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "walknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 5,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 7,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 8,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 9,
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 10,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "attackwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 11,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 13,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 14,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "walkwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 15,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 16,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 17,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 18,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 19,
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 20,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "attacksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 21,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 22,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 23,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 24,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "walksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 25,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 26,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 27,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 28,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 29,
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 30,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "attackeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 31,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 32,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 33,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 34,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "walkeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 35,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 36,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 37,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 38,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 39,
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 40,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true,
            ["collidable"] = false,
            ["name"] = "die"
          },
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            }
          }
        },
        {
          id = 41,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 42,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 43,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 44,
          type = "Enemy"
        },
        {
          id = 45,
          type = "Enemy"
        },
        {
          id = 46,
          type = "Enemy"
        },
        {
          id = 47,
          type = "Enemy"
        },
        {
          id = 48,
          type = "Enemy"
        }
      }
    },
    {
      name = "spider07",
      firstgid = 868,
      filename = "spider/spider07.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider07.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "attacknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 1,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 2,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "walknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 5,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 7,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 8,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 9,
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 10,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "attackwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 11,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 13,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 14,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "walkwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 15,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 16,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 17,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 18,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 19,
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 20,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "attacksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 21,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 22,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 23,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 24,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "walksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 25,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 26,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 27,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 28,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 29,
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 30,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "attackeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 31,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 32,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 33,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 34,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "walkeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 35,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 36,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 37,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 38,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 39,
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 40,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true,
            ["collidable"] = false,
            ["name"] = "die"
          },
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            }
          }
        },
        {
          id = 41,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 42,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 43,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 44,
          type = "Enemy"
        },
        {
          id = 45,
          type = "Enemy"
        },
        {
          id = 46,
          type = "Enemy"
        },
        {
          id = 47,
          type = "Enemy"
        },
        {
          id = 48,
          type = "Enemy"
        }
      }
    },
    {
      name = "spider08",
      firstgid = 918,
      filename = "spider/spider08.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider08.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "attacknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 1,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 2,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "walknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 5,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 7,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 8,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 9,
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 10,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "attackwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 11,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 13,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 14,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "walkwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 15,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 16,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 17,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 18,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 19,
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 20,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "attacksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 21,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 22,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 23,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 24,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "walksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 25,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 26,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 27,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 28,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 29,
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 30,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "attackeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 31,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 32,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 33,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 34,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "walkeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 35,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 36,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 37,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 38,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 39,
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 40,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true,
            ["collidable"] = false,
            ["name"] = "die"
          },
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            }
          }
        },
        {
          id = 41,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 42,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 43,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 44,
          type = "Enemy"
        },
        {
          id = 45,
          type = "Enemy"
        },
        {
          id = 46,
          type = "Enemy"
        },
        {
          id = 47,
          type = "Enemy"
        },
        {
          id = 48,
          type = "Enemy"
        }
      }
    },
    {
      name = "spider09",
      firstgid = 968,
      filename = "spider/spider09.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider09.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "attacknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 1,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 2,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "walknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 5,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 7,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 8,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 9,
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 10,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "attackwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 11,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 13,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 14,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "walkwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 15,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 16,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 17,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 18,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 19,
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 20,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "attacksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 21,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 22,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 23,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 24,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "walksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 25,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 26,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 27,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 28,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 29,
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 30,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "attackeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 31,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 32,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 33,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 34,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "walkeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 35,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 36,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 37,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 38,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 39,
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 40,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true,
            ["collidable"] = false,
            ["name"] = "die"
          },
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            }
          }
        },
        {
          id = 41,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 42,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 43,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 44,
          type = "Enemy"
        },
        {
          id = 45,
          type = "Enemy"
        },
        {
          id = 46,
          type = "Enemy"
        },
        {
          id = 47,
          type = "Enemy"
        },
        {
          id = 48,
          type = "Enemy"
        }
      }
    },
    {
      name = "spider10",
      firstgid = 1018,
      filename = "spider/spider10.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider10.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "attacknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 1,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 2,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "walknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 5,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 7,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 8,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 9,
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 10,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "attackwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 11,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 13,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 14,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "walkwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 15,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 16,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 17,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 18,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 19,
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 20,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "attacksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 21,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 22,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 23,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 24,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "walksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 25,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 26,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 27,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 28,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 29,
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 30,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "attackeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 31,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 32,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 33,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 34,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "walkeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 35,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 36,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 37,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 38,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 39,
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 40,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true,
            ["collidable"] = false,
            ["name"] = "die"
          },
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            }
          }
        },
        {
          id = 41,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 42,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 43,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 44,
          type = "Enemy"
        },
        {
          id = 45,
          type = "Enemy"
        },
        {
          id = 46,
          type = "Enemy"
        },
        {
          id = 47,
          type = "Enemy"
        },
        {
          id = 48,
          type = "Enemy"
        }
      }
    },
    {
      name = "spider11",
      firstgid = 1068,
      filename = "spider/spider11.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider11.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "attacknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 1,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 2,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 3,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 4,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270,
            ["name"] = "walknorth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 5,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 6,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 7,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 8,
          type = "Enemy",
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 9,
          properties = {
            ["faceangle"] = 270
          }
        },
        {
          id = 10,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "attackwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 11,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 12,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 13,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 14,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180,
            ["name"] = "walkwest"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 15,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 16,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 17,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 18,
          type = "Enemy",
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 19,
          properties = {
            ["faceangle"] = 180
          }
        },
        {
          id = 20,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "attacksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 21,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 22,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 23,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 24,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90,
            ["name"] = "walksouth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 25,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 26,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 27,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 28,
          type = "Enemy",
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 29,
          properties = {
            ["faceangle"] = 90
          }
        },
        {
          id = 30,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "attackeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 31,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 32,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 33,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 34,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0,
            ["name"] = "walkeast"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 35,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 36,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 37,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 38,
          type = "Enemy",
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 39,
          properties = {
            ["faceangle"] = 0
          }
        },
        {
          id = 40,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true,
            ["collidable"] = false,
            ["name"] = "die"
          },
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            },
            {
              tileid = 44,
              duration = 33
            },
            {
              tileid = 43,
              duration = 33
            }
          }
        },
        {
          id = 41,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 42,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 43,
          type = "Enemy",
          properties = {
            ["animenddiscard"] = true
          }
        },
        {
          id = 44,
          type = "Enemy"
        },
        {
          id = 45,
          type = "Enemy"
        },
        {
          id = 46,
          type = "Enemy"
        },
        {
          id = 47,
          type = "Enemy"
        },
        {
          id = 48,
          type = "Enemy"
        }
      }
    }
  },
  layers = {
    {
      type = "objectgroup",
      name = "playerbounds",
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {
        ["collidable"] = true,
        ["static"] = true
      },
      objects = {
        {
          id = 3,
          name = "",
          type = "",
          shape = "polyline",
          x = 400,
          y = 80,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          polyline = {
            { x = -320, y = 0 },
            { x = -304, y = 16 },
            { x = -208, y = 16 },
            { x = -192, y = 0 },
            { x = -176, y = 16 },
            { x = -144, y = 16 },
            { x = -128, y = 0 },
            { x = -112, y = 16 },
            { x = -16, y = 16 },
            { x = 0, y = 0 }
          },
          properties = {}
        },
        {
          id = 4,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 0,
          width = 32,
          height = 640,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 5,
          name = "",
          type = "",
          shape = "rectangle",
          x = 32,
          y = 512,
          width = 416,
          height = 128,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 6,
          name = "",
          type = "",
          shape = "rectangle",
          x = 448,
          y = 0,
          width = 32,
          height = 640,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 7,
          name = "",
          type = "",
          shape = "rectangle",
          x = 32,
          y = 0,
          width = 416,
          height = 80,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "tilelayer",
      name = "castlefloors",
      x = 0,
      y = 0,
      width = 15,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBhY4IODj0scBniAmJcEzIekVwyIxUnAEqN6R/UOU71DDQAAYZsRCQ=="
    },
    {
      type = "tilelayer",
      name = "lowerwalls",
      x = 0,
      y = 0,
      width = 15,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYGBg2AnEu4B4NxLeQ0AcBo4C8TE0NScIiMPARSC+BMTHkfAVAuKjYBSMgpELAMO4HbA="
    },
    {
      type = "tilelayer",
      name = "decoration",
      x = 0,
      y = 0,
      width = 15,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYGBgkGRkwApIEVcEiikBsTJUzhhKmwBpUyA2w2EWCKgD5TSAWBOqxhJKWwFpayC2waNXFyinB8T6UDX2UNoBSDsCsRMevYTAIQJ651Fg9nADQyUsphDhTkLxTok91DAbBFYNkfCmNgAAr38J5g=="
    },
    {
      type = "objectgroup",
      name = "player",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 1,
          name = "player",
          type = "",
          shape = "rectangle",
          x = 240,
          y = 304,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 543,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "tilelayer",
      name = "upperwalls",
      x = 0,
      y = 0,
      width = 15,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJw7w0AeOA3EZ0b1juod1Uux3htk6r0OxMVAvJkMXDxEMQBsMTe9"
    },
    {
      type = "objectgroup",
      name = "safeareas",
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 8,
          name = "",
          type = "SafeArea",
          shape = "rectangle",
          x = 16,
          y = 80,
          width = 448,
          height = 448,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "safelimit",
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 16,
          name = "",
          type = "",
          shape = "rectangle",
          x = 16,
          y = 80,
          width = 448,
          height = 448,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "imagelayer",
      name = "void",
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      image = "void/void.png",
      properties = {
        ["script"] = "Void"
      }
    },
    {
      type = "objectgroup",
      name = "enemies",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {}
    },
    {
      type = "tilelayer",
      name = "hudbg",
      x = 0,
      y = 0,
      width = 15,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBgFo2AUjALSgC8F2IcCDAAyYgj4"
    },
    {
      type = "objectgroup",
      name = "hud",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 13,
          name = "message",
          type = "",
          shape = "text",
          x = 16,
          y = 576,
          width = 448,
          height = 64,
          rotation = 0,
          visible = true,
          text = "Arrows=move\nZ=fire X=fire+strafe\nMove or fire to start",
          fontfamily = "PCPaint Bold Medium",
          wrap = true,
          color = { 255, 255, 255 },
          valign = "center",
          properties = {}
        }
      }
    }
  }
}
