<?xml version="1.0" encoding="UTF-8"?>
<tileset name="cast" tilewidth="64" tileheight="64" tilecount="28" columns="7">
 <tileoffset x="-32" y="24"/>
 <properties>
  <property name="commonanimation" value="south"/>
 </properties>
 <image source="cast.png" width="448" height="256"/>
 <tile id="0" type="Player">
  <properties>
   <property name="name" value="north"/>
  </properties>
 </tile>
 <tile id="1" type="Player"/>
 <tile id="2" type="Player"/>
 <tile id="3" type="Player"/>
 <tile id="4" type="Player"/>
 <tile id="5" type="Player"/>
 <tile id="6" type="Player"/>
 <tile id="7" type="Player">
  <properties>
   <property name="name" value="west"/>
  </properties>
 </tile>
 <tile id="8" type="Player"/>
 <tile id="9" type="Player"/>
 <tile id="10" type="Player"/>
 <tile id="11" type="Player"/>
 <tile id="12" type="Player"/>
 <tile id="13" type="Player"/>
 <tile id="14" type="Player">
  <properties>
   <property name="name" value="south"/>
  </properties>
  <animation>
   <frame tileid="14" duration="66"/>
   <frame tileid="15" duration="66"/>
   <frame tileid="16" duration="66"/>
   <frame tileid="17" duration="66"/>
   <frame tileid="18" duration="66"/>
   <frame tileid="19" duration="604"/>
   <frame tileid="20" duration="66"/>
  </animation>
 </tile>
 <tile id="15" type="Player"/>
 <tile id="16" type="Player"/>
 <tile id="17" type="Player"/>
 <tile id="18" type="Player"/>
 <tile id="19" type="Player"/>
 <tile id="20" type="Player"/>
 <tile id="21" type="Player">
  <properties>
   <property name="name" value="east"/>
  </properties>
 </tile>
 <tile id="22" type="Player"/>
 <tile id="23" type="Player"/>
 <tile id="24" type="Player"/>
 <tile id="25" type="Player"/>
 <tile id="26" type="Player"/>
 <tile id="27" type="Player"/>
</tileset>
